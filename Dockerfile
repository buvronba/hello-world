FROM trafex/php-nginx:latest

# Set the working directory in the container
WORKDIR /var/www/html

# Copy the PHP application into the container
COPY index.php /var/www/html/

# The base image already exposes port 8080, so we don't need to expose it again